package rented.vehicle;

public class FuelVehicle extends RentedVehicle {
	private double nbkms;
	
	
	public FuelVehicle(double baseFee, double nbkms) {
		super(baseFee);
		this.nbkms=nbkms;
	}
	
	public double getMileageFees() {
		if(nbkms<100) {
			return 0.2*nbkms;
		}
		else if(nbkms<400||nbkms>=100) {
			return 0.3*nbkms;
		}
		else {
			return 0.3*400+(nbkms-400)*0.5;
		}
	}
	
}