package rented.vehicle;

public class Car extends FuelVehicle {

	private int nbSeats;
	
	public Car(double basefee, double nbKms, int nbSeats) {
		super(basefee, nbKms);
		this.nbSeats = nbSeats;
		}


	public double getCost(double basefee) {
		if(nbSeats < 4) {
			return nbSeats*basefee;
		}
		else
		
		return getMileageFees();
	}

	
	

}
