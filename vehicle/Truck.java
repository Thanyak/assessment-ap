package rented.vehicle;

public class Truck extends FuelVehicle {
	
	private int capacity;
	
	public Truck(double baseFee, double nbkms, int capacity) {
		super(baseFee, nbkms);
		this.capacity = capacity;
	}

	public double getCost(double baseFee) {
		if (capacity < 16.7 ) {
			return baseFee*capacity;
		}
		else
			return capacity;
	}
}
