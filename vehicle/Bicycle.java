package rented.vehicle;

public class Bicycle extends RentedVehicle{
	
	private int nbDays;
	
	public Bicycle(double baseFee, int nbDays) {
		super(baseFee);
		this.nbDays = nbDays;
	}

	public double getCost(double baseFee) {
		if(nbDays<1) {
		return baseFee*nbDays;
		}
		else {
			return baseFee;
		}
	}

	public void setNbDays(int nbDays) {
		this.nbDays = nbDays;
	}

}
