package rented.vehicle;

public class RentedVehicle {

	private double baseFee;

	public RentedVehicle(double baseFee) {
		super();
	}
	
	public double getCost() {
		return baseFee;
	}

}
