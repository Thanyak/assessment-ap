package basic.terminologies;

public interface Interface {

	// interface
	interface Book {
	  public void bookpage(); // interface method (does not have a body)
	  public void type(); // interface method (does not have a body)
	}
}

