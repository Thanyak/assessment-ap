package basic.terminologies;

public class Method {
	
	static void myMethod()
	{
		//Pre_defined method
		System.out.println("It is a method!");
	}

	public static void main(String[] args) 
	{
		myMethod();
		myMethod();
	}

}
