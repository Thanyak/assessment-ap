package basic.terminologies;

public class Constructor_Demo {
	
public static void main(String[] args) {
		Constructor object=new Constructor();
		 System.out.println(object.name);
		 System.out.println(object.id+"\n");
		 
		Constructor object1 = new Constructor("Thanya", 26);
		 System.out.println("Name: " + object1.name );
		 System.out.println("id: " + object1.id+"\n");
		    
		Constructor object2 = new Constructor("Kajan", 34);
		 System.out.println("Name: " + object2.name );
		 System.out.println("id: " + object2.id+"\n");
		
	}

}
