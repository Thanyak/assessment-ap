package principles.of.oop;

public abstract class Phone {
	private String name;
	private String model;
	
	public Phone(String name, String model) {
		this.name=name;
		this.model=model;
	}
	
	//abstract method
		public abstract void call();
		public abstract void message();
		public abstract void game();
		
		
		public String toString() {
			return "Name : "+ this.name+"\r\n"+"Model : "+this.model;
			
		}
		
}
