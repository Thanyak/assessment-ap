package principles.of.oop;

public class BookEncapDemo {

	public static void main(String[] args) {
		Book BeautifulWorld=new Book();
		BeautifulWorld.setName("Beautiful World");
		BeautifulWorld.setAuthor("Thanu");
		BeautifulWorld.setPrice(450.00);
		BeautifulWorld.setPages(200);
		
		Book Angel=new Book();
		Angel.setName("The Angel");
		Angel.setAuthor("Rathi");
		Angel.setPrice(500.00);
		Angel.setPages(250);
		
		System.out.println(BeautifulWorld);
		System.out.println(Angel);
		
		System.out.println(BeautifulWorld.getName()+"-"+BeautifulWorld.getPrice()+"/=");
		System.out.println(Angel.getName()+"-"+Angel.getPrice()+"/=");
		
	}

}
