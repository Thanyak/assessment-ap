package principles.of.oop;

public class Book {
	
	private String name;
	private String author;
	private double price;
	private int pages;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}


	@Override
	public String toString() {
		return "Book[Name:"+name+"   "+"Author:"+author+"   "+"Price:"+price+"/="+"   "+"Pages:"+pages+"]";
	}

}
