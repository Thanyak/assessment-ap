package online.shopping.creational.singleton;

public class Customer {

	private String id;
	private String name;
	private Address address;
	private Phone phone;
	private String email;
	
	Customer() {
	
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Phone getPhone() {
		return phone;
	}

	public void setPhone(Phone phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	@Override
	public String toString() {
		
		return "Customer[Id:"+id+" , "+"Name:"+name+" , "+"Address:"+address+" , "+"Phone:"+phone+" , "+"Email:"+email+"]"+"\n";
	
	}

}
