package online.shopping.creational.singleton;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class DBQuery implements DataManipulation {

private static DBQuery dbQuery;
	
	private DBQuery() {
	
	}
	
	public static DBQuery getTnstance() {
		if(dbQuery==null) {
		dbQuery=new DBQuery();
		}
		return dbQuery;
	}
	
	
	//To get connection from methods like insert data/ view data etc.
		private Connection getConnection()throws ClassNotFoundException,SQLException{
			Connection connection=null;
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/online_shopping","root","root");
			return connection;
		}
	
	public void insertData(String name, String address, String phone, String email) throws SQLException {
		
		Connection connection=null;
		PreparedStatement statement=null;
	
		try {
			connection=this.getConnection();
			statement=connection.prepareStatement("INSERT INTO customer_details(Name, Address, Phone, Email)VALUES(?,?)");
			
			
			statement.setString(1, name);
			statement.setString(2, address);
			statement.setString(3, phone);
			statement.setString(4, email);
			
			statement.executeUpdate();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			if(connection !=null) {
				connection.close();
			}
			if(statement !=null) {
				statement.close();
			}
		}
	}


	public void readData(int searchInt) throws SQLException {

		Connection connection=null;
		PreparedStatement statement=null;
		ResultSet resultSet=null;
	
		try {
			connection=this.getConnection();
			statement=connection.prepareStatement("SELECT * FROM customer_details WHERE Customer_Id=?");
			
			statement.setInt(0, searchInt);
			resultSet=statement.executeQuery();
			
			System.out.println("ID\tName");
			while (resultSet.next()){
				System.out.println(resultSet.getString(1)+"\t"+resultSet.getString(2)+"\t"+"\t"+resultSet.getString(3)+"\t"+"\t"+"\t"+resultSet.getString(4)+"\t"+"\t"+"\t"+resultSet.getString(5));
			}
						
		}catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			if(resultSet !=null) {
				resultSet.close();
			}
			if(connection !=null) {
				connection.close();
			}
			if(statement !=null) {
				statement.close();
			}
		}
	}





	public void updateData(int id, String name, String address, String phone, String email) throws SQLException {
	
		Connection connection=null;
		PreparedStatement statement=null;
	
		try {
			connection=this.getConnection();
			statement=connection.prepareStatement("UPDATE customer_details SET Name, Address, Phone, Email=? WHERE Customer_Id=?");
			statement.setString(1, name);
			statement.setString(2, address);
			statement.setString(3, phone);
			statement.setString(4, email);
			
			statement.executeUpdate();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			if(connection !=null) {
				connection.close();
			}
			if(statement !=null) {
				statement.close();
			}
		}
		
	}


	public void deleteData(int id) throws SQLException {
		
		Connection connection=null;
		PreparedStatement statement=null;
	
		try {
			connection=this.getConnection();
			statement=connection.prepareStatement("DELETE FROM customer_details WHERE Customer_Id=?");
			statement.setInt(0, id);
			statement.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			if(connection !=null) {
				connection.close();
			}
			if(statement !=null) {
				statement.close();
			}
		}
	}


	

	

}
