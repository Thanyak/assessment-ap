package online.shopping.creational.singleton;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

public class ShoppingCart {
	 	private String Id;
	    private Web_User user;
	    private Set<LineItem> LineItem=new HashSet<>();
	    private Date date;
	    
	    public ShoppingCart() {
	    	
	    }
	    
	    public String getId() {
			return Id;
		}
		public void setId(String id) {
			Id = id;
		}
		
		public Web_User getUser() {
			return user;
		}
		public void setUser(Web_User user) {
			this.user = user;
		}
		public Set<LineItem> getLineItem() {
			return LineItem;
		}
		public void setLineItem(Set<LineItem> lineItem) {
			LineItem = lineItem;
		}
		public Date getDate() {
			return date;
		}
		public void setDate(Date date) {
			this.date = date;
		}

}
