package online.shopping.creational.singleton;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBC_Connector {

	public static void main(String[] args) {
	
			System.out.println("------MYSQL JDBC CONNECTION TEST------");

		
		try {
		Class.forName("com.mysql.cj.jdbc.Driver");
		System.out.println("JDBC driver is attached");
		}catch(ClassNotFoundException e) {
			System.err.println("JDBC driver is not attached");
			e.printStackTrace();
		}
		
		Connection connection=null;
		
		try {
			connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/online_shopping","root","root");
		}catch(SQLException e) {
			e.printStackTrace();
		}
		
		if (connection!=null) {
			System.out.println("Sucess!!! Database was connected");
		}
		else {
			System.out.println("Failed to make connection");
		}
		
		}

	


}
