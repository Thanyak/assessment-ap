package online.shopping.creational.singleton;

	public class Web_User extends Customer {

		private String login_id;
	    private String password;
	    private Enum<UserState> userstate;
	    
	Web_User(){
		
	}

	public Web_User(String login_id, String password,UserState userstate) {
        this.setLogin_id(login_id);
        this.password = password;
        this.userstate=userstate;
    }
	
	
	public String getLogin_id() {
		return login_id;
	}

	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Enum<UserState> getUserstate() {
		return userstate;
	}

	public void setUserstate(Enum<UserState> userstate) {
		this.userstate = userstate;
	}

	public String toString() {
		
		return "Product[Id:"+login_id+" , "+"Password:"+password+" , "+"State:"+userstate+"\n";
	
	}

	
}