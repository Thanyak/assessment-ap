package online.shopping.creational.singleton;

public class Product {
	
	private String id;
	private String name;
	private String supplier;
	private Price price;

	public Product() {
		
	}
	
	public Product(String id, String name, String supplier, Price price) {
		this.id = id;
		this.name = name;
		this.supplier = supplier;
		this.price = price;
	}
	
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSupplier() {
		return supplier;
	}


	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}


	public Price getPrice() {
		return price;
	}


	public void setPrice(Price price) {
		this.price = price;
	}
	
	public String toString() {
		
		return "Product[Id:"+id+" , "+"Name:"+name+" , "+"Supplier:"+supplier+" , "+"Price:"+price+"]"+"\n";
	
	}
}