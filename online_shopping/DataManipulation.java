package online.shopping.creational.singleton;

import java.sql.SQLException;

public interface DataManipulation {
	public void insertData(String name,String address,String phone, String email) throws SQLException;
	public void readData(int searchInt)throws SQLException;
	public void updateData(int id, String name,String address,String phone, String email) throws SQLException;
	public void deleteData(int id) throws SQLException;
	
	
}
