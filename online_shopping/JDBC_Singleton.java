package online.shopping.creational.singleton;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JDBC_Singleton {
	
	
	//create a JDBCSingleton class.
	//static members holds only one instance of the JDBCSingleton class.
	private static JDBC_Singleton jdbc;
	
	//JDBCSingleton prevent the instantiation from any other class.
	private JDBC_Singleton() {
	}
	
	//Here we provides global point of access.
	public static JDBC_Singleton getTnstance() {
		if(jdbc==null) {
			jdbc=new JDBC_Singleton();
		}
		return jdbc;
	}
	
	//To get connection from methods like insert data/ view data etc.
	private Connection getConnection()throws ClassNotFoundException,SQLException{
		Connection connection=null;
		Class.forName("com.mysql.cj.jdbc.Driver");
		connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/online_shopping","root","root");
		return connection;
	}
	
	//To insert the record into the database.
	public void inserData(int id, String name,String address,String phone, String email)throws SQLException{
		Connection connection=null;
		PreparedStatement statement=null;
		
		try{
			connection=this.getConnection();
	
			statement=connection.prepareStatement("INSERT INTO customer_details(Customer_Id, Name, Address, Phone, Email)VALUES(?,?)");
			
			statement.setInt(1, id);
			statement.setString(2, name);
			statement.setString(3, address);
			statement.setString(4, phone);
			statement.setString(5, email);
			
			statement.executeUpdate();
			
		}catch (ClassNotFoundException|SQLException e) {
			e.printStackTrace();
		}finally {
			if (connection !=null) {
				connection.close();
			}
			if(statement !=null) {
				statement.close();
			}
		}
		
	}
	

	//To read the record from the database.
	public void readData() throws SQLException {
		Connection connection=null;
		PreparedStatement statement=null;
		ResultSet resultSet=null;
	
		try {
			connection=this.getConnection();
			statement=connection.prepareStatement("SELECT * FROM customer_details");
		
			resultSet=statement.executeQuery();
			System.out.println(" Customer ID\tName\t\tAddres\t\t\tPhone\t\t\tEmail");
			while (resultSet.next()){
				System.out.println(resultSet.getString(1)+"\t"+resultSet.getString(2)+"\t"+"\t"+resultSet.getString(3)+"\t"+"\t"+"\t"+resultSet.getString(4)+"\t"+"\t"+"\t"+resultSet.getString(5));
			}
						
		}catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			if(resultSet !=null) {
				resultSet.close();
			}
			if(connection !=null) {
				connection.close();
			}
			if(statement !=null) {
				statement.close();
			}
		}
		
	}
	
	//To read the record from the database with search parm.
	public void readData(int searchInt) throws SQLException {
		Connection connection=null;
		PreparedStatement statement=null;
		ResultSet resultSet=null;
	
		try {
			connection=this.getConnection();
			statement=connection.prepareStatement("SELECT * FROM customer_details WHERE Customer_Id=?");
			statement.setInt(1, searchInt);
			resultSet=statement.executeQuery();
			System.out.println("ID\tName");
			while (resultSet.next()){
				System.out.println(resultSet.getString(1)+"\t"+resultSet.getString(2));
			}
						
		}catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			if(resultSet !=null) {
				resultSet.close();
			}
			if(connection !=null) {
				connection.close();
			}
			if(statement !=null) {
				statement.close();
			}
		}
		
	}
	
	public void updateData(int id, String name,String address,String phone, String email) throws SQLException {
		Connection connection=null;
		PreparedStatement statement=null;
	
		try {
			connection=this.getConnection();
			statement=connection.prepareStatement("UPDATE customer_details SET Name, Address, Phone, Email=? WHERE Customer_Id=?");
			statement.setString(2, name);
			statement.setString(3, address);
			statement.setString(4, phone);
			statement.setString(5, email);
			
			statement.executeUpdate();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}finally {
			if(connection !=null) {
				connection.close();
			}
			if(statement !=null) {
				statement.close();
			}
		}
	
		
	}
}