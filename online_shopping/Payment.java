package online.shopping.creational.singleton;

import java.sql.Date;

public class Payment {
	private String id;
	private Date paid;
	private Real total;
	private String detail;
	
	public Payment() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getPaid() {
		return paid;
	}

	public void setPaid(Date paid) {
		this.paid = paid;
	}

	public Real getTotal() {
		return total;
	}

	public void setTotal(Real total) {
		this.total = total;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}
	
	
}
