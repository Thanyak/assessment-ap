package online.shopping.creational.singleton;

public class LineItem {
	private String Id;
    private ShoppingCart cart;
    private Product product;
    private int quantity;
    private Price price;
    
    
    public LineItem( ShoppingCart cart, Product product, int quantity,Price price) {
        this.cart = cart;
        this.product = product;
        this.quantity = quantity;
        this.price=price;
    }

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}


	public ShoppingCart getCart() {
		return cart;
	}


	public void setCart(ShoppingCart cart) {
		this.cart = cart;
	}


	public Product getProduct() {
		return product;
	}


	public void setProduct(Product product) {
		this.product = product;
	}


	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public Price getPrice() {
		return price;
	}


	public void setPrice(Price price) {
		this.price = price;
	}
    
    
}
